---
title: "Préparation des données BIEN"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
# knitr::opts_chunk$set(results = "hide")
# knitr::opts_chunk$set(cache = TRUE)
# knitr::opts_chunk$set(eval = FALSE)
# knitr::opts_chunk$set(warning = FALSE)
```


## Chargement des librairies
```{r}
library(stats)
library(sf)
library(dbscan)
library(mapview)
library(factoextra)
library(ggplot2)
library(readxl)
library (openxlsx)
library(tidyverse)
library(naniar)
library(SpatialPosition)
library(cartography)
library(gtools)

```

## Import des données
```{r Import data}

# Import des données No Open Access

## Import des données BIEN externes au projet
BIEN_Ext <- read.csv2("APCP_NoOpenDataRaws/BIEN_9618_Consolidated.csv", stringsAsFactors=FALSE)

#selection commune sd'études
BIEN_Years1<- BIEN_Ext%>% 
  filter(insee==93072|insee==77305| insee==91174) %>% #communes concernees
  filter(annee != 2015) %>%# transactions de l'année 2015 dans l'échantillon interne du projet
  select(-ID) # Colonne Id transactions enlevées pour prochain bind
# 6046 transactions pour les années concernées (elles seront duplique pour redressement de l'échantillon, sule 50% des transactions de la bd BIEN)
rm(BIEN_Ext)

length(which(BIEN_Years1$X<100)) # apriori, 368 transactions de cet échnatillon sans geoloc attribuées
 
 BIEN_Years1$ACONST <- as.numeric(BIEN_Years1$ACONST)
BIEN_Years1$ANNAIS_AC <- as.numeric(BIEN_Years1$ANNAIS_AC)
BIEN_Years1$ANNAIS_VE <- as.numeric(BIEN_Years1$ANNAIS_VE)
BIEN_Years1$annee <- as.numeric(BIEN_Years1$annee)
BIEN_Years1$BIARRON <- as.numeric(BIEN_Years1$BIARRON)
BIEN_Years1$BIDEPT <- as.numeric(BIEN_Years1$BIDEPT)
BIEN_Years1$BINRQUARAD <- as.numeric(BIEN_Years1$BINRQUARAD)
BIEN_Years1$BINRVOIE <- as.numeric(BIEN_Years1$BINRVOIE)
BIEN_Years1$BINUCOM <- as.numeric(BIEN_Years1$BINUCOM)
BIEN_Years1$CAVE <- as.numeric(BIEN_Years1$CAVE)
BIEN_Years1$CSP_AC <- as.numeric(BIEN_Years1$CSP_AC)
BIEN_Years1$CSP_VE <- as.numeric(BIEN_Years1$CSP_VE)
BIEN_Years1$DURBAIL <- as.numeric(BIEN_Years1$DURBAIL)
BIEN_Years1$ETAGE <- as.numeric(BIEN_Years1$ETAGE)
BIEN_Years1$IMSURFTOTB <- as.numeric(BIEN_Years1$IMSURFTOTB)
BIEN_Years1$insee <- as.numeric(BIEN_Years1$insee)
BIEN_Years1$IRIS <- as.numeric(BIEN_Years1$IRIS)
BIEN_Years1$LARGFAC <- as.numeric(BIEN_Years1$LARGFAC)
BIEN_Years1$LOYANNU <- as.numeric(BIEN_Years1$LOYANNU)
BIEN_Years1$mois <- as.numeric(BIEN_Years1$mois)
BIEN_Years1$MTCRED <- as.numeric(BIEN_Years1$MTCRED)
BIEN_Years1$NBRBAT <- as.numeric(BIEN_Years1$NBRBAT)
BIEN_Years1$NBRCHSERV <- as.numeric(BIEN_Years1$NBRCHSERV)
BIEN_Years1$NBRGARAGE <- as.numeric(BIEN_Years1$NBRGARAGE)
BIEN_Years1$NBRPIECE <- as.numeric(BIEN_Years1$NBRPIECE)
BIEN_Years1$NBRSALDB <- as.numeric(BIEN_Years1$NBRSALDB)
BIEN_Years1$NIVEAU <- as.numeric(BIEN_Years1$NIVEAU)
BIEN_Years1$NUMCOM_AC <- as.numeric(BIEN_Years1$NUMCOM_AC)
BIEN_Years1$NUMCOM_VE <- as.numeric(BIEN_Years1$NUMCOM_VE)
BIEN_Years1$PXMUTPREC <- as.numeric(BIEN_Years1$PXMUTPREC)
BIEN_Years1$REQ_ANC <- as.numeric(BIEN_Years1$REQ_ANC)
BIEN_Years1$REQ_COS <- as.numeric(BIEN_Years1$REQ_COS)
BIEN_Years1$REQ_DUREE <- as.numeric(BIEN_Years1$REQ_DUREE)
BIEN_Years1$REQ_MUT <- as.numeric(BIEN_Years1$REQ_MUT)
BIEN_Years1$REQ_OCC <- as.numeric(BIEN_Years1$REQ_OCC)
BIEN_Years1$REQ_PM2 <- as.numeric(BIEN_Years1$REQ_PM2)
BIEN_Years1$REQ_POS <- as.numeric(BIEN_Years1$REQ_POS)
BIEN_Years1$REQ_PRIX <- as.numeric(BIEN_Years1$REQ_PRIX)
BIEN_Years1$REQ_SURFT <- as.numeric(BIEN_Years1$REQ_SURFT)
BIEN_Years1$REQ_VALUE <- as.numeric(BIEN_Years1$REQ_VALUE)
BIEN_Years1$SDHOP	<- as.numeric(BIEN_Years1$SDHOP)
BIEN_Years1$SHON <- as.numeric(BIEN_Years1$SHON)
BIEN_Years1$SURFHABDEC <- as.numeric(BIEN_Years1$SURFHABDEC)
BIEN_Years1$TXDRMUT1 <- as.numeric(BIEN_Years1$TXDRMUT1)
BIEN_Years1$X <- as.numeric(BIEN_Years1$X)
BIEN_Years1$Y <- as.numeric(BIEN_Years1$Y)

## Import des données BIEN internes au projet Il s'agit d'un fichier .dat
BIEN_Int <-  read_delim("APCP_NoOpenDataRaws/2018-04-23_EXTRAC_BASE_LABEX.dat", 
    "\t", escape_double = FALSE, locale = locale(date_names = "fr", 
        decimal_mark = ","), trim_ws = TRUE)
# 3969 transactions 2013-2017 sur les trois communes
#Attention a import sur la corruption des données notamment coords

BIEN_Years2<- BIEN_Int%>% 
  filter(insee==93072|insee==77305| insee==91174) %>%
  select(-ID)

BIEN_Years2$X<-as.numeric(BIEN_Years2$X)
length(which(is.na(BIEN_Years2$X))) # Ici 120 transactions sans coord attribuees
#  Check noms colonnes, apriori mêmes noms


 BIEN_Years2$ACONST <- as.numeric(BIEN_Years2$ACONST)
BIEN_Years2$ANNAIS_AC <- as.numeric(BIEN_Years2$ANNAIS_AC)
BIEN_Years2$ANNAIS_VE <- as.numeric(BIEN_Years2$ANNAIS_VE)
BIEN_Years2$annee <- as.numeric(BIEN_Years2$annee)
BIEN_Years2$BIARRON <- as.numeric(BIEN_Years2$BIARRON)
BIEN_Years2$BIDEPT <- as.numeric(BIEN_Years2$BIDEPT)
BIEN_Years2$BINRQUARAD <- as.numeric(BIEN_Years2$BINRQUARAD)
BIEN_Years2$BINRVOIE <- as.numeric(BIEN_Years2$BINRVOIE)
BIEN_Years2$BINUCOM <- as.numeric(BIEN_Years2$BINUCOM)
BIEN_Years2$CAVE <- as.numeric(BIEN_Years2$CAVE)
BIEN_Years2$CSP_AC <- as.numeric(BIEN_Years2$CSP_AC)
BIEN_Years2$CSP_VE <- as.numeric(BIEN_Years2$CSP_VE)
BIEN_Years2$DURBAIL <- as.numeric(BIEN_Years2$DURBAIL)
BIEN_Years2$ETAGE <- as.numeric(BIEN_Years2$ETAGE)
BIEN_Years2$IMSURFTOTB <- as.numeric(BIEN_Years2$IMSURFTOTB)
BIEN_Years2$insee <- as.numeric(BIEN_Years2$insee)
BIEN_Years2$IRIS <- as.numeric(BIEN_Years2$IRIS)
BIEN_Years2$LARGFAC <- as.numeric(BIEN_Years2$LARGFAC)
BIEN_Years2$LOYANNU <- as.numeric(BIEN_Years2$LOYANNU)
BIEN_Years2$mois <- as.numeric(BIEN_Years2$mois)
BIEN_Years2$MTCRED <- as.numeric(BIEN_Years2$MTCRED)
BIEN_Years2$NBRBAT <- as.numeric(BIEN_Years2$NBRBAT)
BIEN_Years2$NBRCHSERV <- as.numeric(BIEN_Years2$NBRCHSERV)
BIEN_Years2$NBRGARAGE <- as.numeric(BIEN_Years2$NBRGARAGE)
BIEN_Years2$NBRPIECE <- as.numeric(BIEN_Years2$NBRPIECE)
BIEN_Years2$NBRSALDB <- as.numeric(BIEN_Years2$NBRSALDB)
BIEN_Years2$NIVEAU <- as.numeric(BIEN_Years2$NIVEAU)
BIEN_Years2$NUMCOM_AC <- as.numeric(BIEN_Years2$NUMCOM_AC)
BIEN_Years2$NUMCOM_VE <- as.numeric(BIEN_Years2$NUMCOM_VE)
BIEN_Years2$PXMUTPREC <- as.numeric(BIEN_Years2$PXMUTPREC)
BIEN_Years2$REQ_ANC <- as.numeric(BIEN_Years2$REQ_ANC)
BIEN_Years2$REQ_COS <- as.numeric(BIEN_Years2$REQ_COS)
BIEN_Years2$REQ_DUREE <- as.numeric(BIEN_Years2$REQ_DUREE)
BIEN_Years2$REQ_MUT <- as.numeric(BIEN_Years2$REQ_MUT)
BIEN_Years2$REQ_OCC <- as.numeric(BIEN_Years2$REQ_OCC)
BIEN_Years2$REQ_PM2 <- as.numeric(BIEN_Years2$REQ_PM2)
BIEN_Years2$REQ_POS <- as.numeric(BIEN_Years2$REQ_POS)
BIEN_Years2$REQ_PRIX <- as.numeric(BIEN_Years2$REQ_PRIX)
BIEN_Years2$REQ_SURFT <- as.numeric(BIEN_Years2$REQ_SURFT)
BIEN_Years2$REQ_VALUE <- as.numeric(BIEN_Years2$REQ_VALUE)
BIEN_Years2$SDHOP	<- as.numeric(BIEN_Years2$SDHOP)
BIEN_Years2$SHON <- as.numeric(BIEN_Years2$SHON)
BIEN_Years2$SURFHABDEC <- as.numeric(BIEN_Years2$SURFHABDEC)
BIEN_Years2$TXDRMUT1 <- as.numeric(BIEN_Years2$TXDRMUT1)
BIEN_Years2$X <- as.numeric(BIEN_Years2$X)
BIEN_Years2$Y <- as.numeric(BIEN_Years2$Y)

# bind_rows(BIEN_Years2, BIEN_Years1) # Problème de bindage qui vient du type de variables

# fonction de gtools
# BIEN_BrutCom <- smartbind (BIEN_Years1, BIEN_Years2) # Les colonnes avec types différents sont converties en charateres.
# N = 10015 transactions
BIEN_BrutCom<-bind_rows(BIEN_Years1, BIEN_Years2)
BIEN_BrutCom <- BIEN_BrutCom %>%
arrange(annee, insee)
  
rownames(BIEN_BrutCom) <- 1:nrow(BIEN_BrutCom)
BIEN_BrutCom$ID <- as.numeric(row.names(BIEN_BrutCom))

BIEN_BrutCom<- BIEN_BrutCom%>%
  relocate (ID)

```


## Geocodage

```{r Geocod}

# Import du cadastre des 3 communes
parcelles <- st_read("APCP_Geom/APCP_Geom.gpkg", quiet = TRUE, layer = "parcelles") 


# Creation de champ X et Y pour la table des parcelles a partir du centroide 
centrpar <- as.data.frame(st_coordinates(st_centroid(st_geometry(parcelles))))
parcelles["X"] <- centrpar$X
parcelles["Y"] <- centrpar$Y
rm(centrpar)

## Creation d'un ID concatene pour chaque parcelle a partir de code INSEE de la commune, de la section cadastrale et du numero de parcelle
### On ne choisit pas le prefixe de section cadastrale, car il n'est pas renseigne sur BIEN 
### NB : Si on a travaille sur des donnees parcellaires anciennes, les champs de commune, section et numero ne sont pas forcement renseignes
### Pour que le script fonctionne avec toutes les annees sans soucis, on les extrait donc a partir de l'ID de la parcelle, present pour chaque version
parcelles["commune"] <- substr(parcelles$idpar,start=1,stop=5)
parcelles["prefixe"] <- substr(parcelles$idpar,start=6,stop=8)
parcelles["section"] <- substr(parcelles$idpar,start=9,stop=10)
parcelles["numero"] <- substr(parcelles$idpar,start=11,stop=14)


parcelles["IDCONCAT"] <- paste(parcelles$commune,parcelles$section,parcelles$numero, sep="")

nrow(parcelles[which(parcelles$prefixe != '000'),])
head(parcelles)
## Creation d'une couche section parcellaire a partir de la couche parcelles
### Creation d'un ID concatene de la section cadastrale, pour la jointure avec BIEN
parcelles["IDCONCATSEC"] <- paste(parcelles$commune,parcelles$prefixe,parcelles$section, sep="")


#### Attribution coordonnées géographiques à partir des centroïdes des parcelles ####


BIEN_Coord <- BIEN_BrutCom[,c("ID","NRPLAN1","REFSECTION","insee","REQTYPBIEN","X","Y")]

# Creation d'un ID concatene pour chaque ligne de BIEN a partir du code INSEE de la commune, de la section cadastrale et du numero de parcelle
colnames(BIEN_Coord) <- c("ID","num_plan","cod_section","num_cominsee","REQTYPBIEN","x","y")
BIEN_Coord$x <- as.numeric(BIEN_Coord$x)
BIEN_Coord$y <- as.numeric(BIEN_Coord$y)
## Il faut passer par des etapes intermediaires afin de faire correspondre les formats entre les parcelles et BIEN, surtout au niveau des numeros de parcelle et de la section cadastrale

### Rajouts de 0 si necessaire afin de mettre le numero de parcelle sur 4 caracteres, et le code de section cadastrale sur 2 caracteres

#### On converti les colonnes num_plan et cod_section en chaine de caractere, au lieu de facteur
BIEN_Coord$num_plan <- as.character(BIEN_Coord$num_plan)

BIEN_Coord$cod_section <- as.character(BIEN_Coord$cod_section)

#### Rajout des 0 lorsque necessaire
BIEN_Coord <- BIEN_Coord%>%
  mutate( num_plan = case_when(is.na(num_plan)~"ERREUR", TRUE ~num_plan))

BIEN_Coord$num_plan <- if_else(nchar(as.character(BIEN_Coord$num_plan)) == 1, as.character(paste("000",BIEN_Coord$num_plan, sep="")), 
if_else(nchar(as.character(BIEN_Coord$num_plan)) == 2, as.character(paste("00",BIEN_Coord$num_plan, sep="")),
if_else(nchar(as.character(BIEN_Coord$num_plan)) == 3, as.character(paste("0",BIEN_Coord$num_plan, sep="")),as.character(BIEN_Coord$num_plan))))

BIEN_Coord <- BIEN_Coord%>%
  mutate( cod_section = case_when(is.na(cod_section)~"ERREUR", TRUE ~cod_section))

BIEN_Coord$cod_section <- if_else(nchar(as.character(BIEN_Coord$cod_section)) == 1, as.character(paste("0",BIEN_Coord$cod_section, sep="")),as.character(BIEN_Coord$cod_section))

nchar(as.character(parcelles$IDCONCATSEC)) == 10
#### Numero de parcelle
# for(i in 1:nrow(BIEN)) {
#   if(is.na(BIEN[i,"num_plan"])) { BIEN[i,"num_plan"] <- as.character("ERREUR") }
#   else if(nchar(as.character(BIEN[i,"num_plan"])) == 1) {
#     BIEN[i,"num_plan"] <- as.character(paste("000",BIEN[i,"num_plan"], sep=""))
#   }
#   else if(nchar(as.character(BIEN[i,"num_plan"])) == 2) {
#     BIEN[i,"num_plan"] <- as.character(paste("00",BIEN[i,"num_plan"], sep=""))
#   }
#   else if(nchar(as.character(BIEN[i,"num_plan"])) == 3) {
#     BIEN[i,"num_plan"] <- as.character(paste("0",BIEN[i,"num_plan"], sep=""))
#   }
# }

## Code de section cadastrale
# for(i in 1:nrow(BIEN)) {
#   if(is.na(BIEN[i,"cod_section"])) { BIEN[i,"cod_section"] <- as.character("ERREUR") }
#   else if(nchar(as.character(BIEN[i,"cod_section"])) == 1) {
#     BIEN[i,"cod_section"] <- as.character(paste("0",BIEN[i,"cod_section"], sep=""))
#   }
# }

# On peut alors creer l'ID concatene qui va permettre de faire la jointure avec la table des parcelles
BIEN_Coord["IDCONCAT"] <- paste(BIEN_Coord$num_cominsee,BIEN_Coord$cod_section,BIEN_Coord$num_plan, sep="")

# Creation ID concatene qui va permettre de faire la jointure avec la table des sections si necessaire
#Prefixe
BIEN_Coord$prefixe_sec <- "000"
BIEN_Coord["IDCONCATSEC"] <- paste(BIEN_Coord$num_cominsee,BIEN_Coord$prefixe_sec,BIEN_Coord$cod_section, sep="")

length(which(BIEN_Coord$num_plan=="ERREUR"))
# Résulat intermédiaire : problème d'encodage des attributs parcellaires pour au moins 5 transactions

### Jointure  entre BIEN et la couche des parcelles
BIEN_Coord_Parcelles<- BIEN_Coord %>%
  left_join(., as.data.frame(parcelles)%>%
              select(-geom), 
            by = "IDCONCAT") %>%
  filter(!duplicated(ID))


unique(duplicated(BIEN_Coord_Parcelles$ID))
length(which(is.na(BIEN_Coord_Parcelles$X)))# 61 transactions sans parcelle identifée ni coords d'origine


BIEN_AjustCoord <- BIEN_Coord_Parcelles%>%
  rename(X_parcelles = X) %>%
  rename(Y_parcelles = Y) 


BIEN_AjustCoord <- BIEN_AjustCoord %>%
  mutate(Result_Coords = case_when((x < 100 | is.na(x)) & !is.na(X_parcelles)~ "NoOriginalsCoords_ParcellesCoords", 
                                   (x < 100 | is.na(x))  & is.na(X_parcelles)~ "NoOriginalsCoords_NoParcellesCoords",
                                   x > 100 & is.na(X_parcelles)~ "OriginalsCoords_NoParcellesCoords",
                                   x > 100 & !is.na(X_parcelles)~ "OriginalsCoords_ParcellesCoords"))

table(BIEN_AjustCoord$Result_Coords, useNA = "always")

# Résultats : sur 46717 lignes sans coords X,Y et erreurs ; 7 719 n'ont pas d'attribution avec les parcelles (informations manquantes, d'autres erronnées, actualisation du cadastre ?) : ces transactions sont écartées de l'analyse
# 80 522 lignes avec coordonnées sans attributs directs avec la parcelle cadastrale : on garde les coordonnées d'origine 
# 43 547 lignes sans coordonnéees avec attributs directs avec la parcelle cadastrale : on stocke les coordonnées de la parcelle cadastrale comme coordonnées des transactions
# 982 007 lignes avec coordonnées et attributs directs avec la parcelle cadastrale : on stocke les coordonnées de la parcelle cadastrale comme coordonnées des transactions

BIEN_AjustCoord <- BIEN_AjustCoord%>%
  select(ID,x, y, X_parcelles,Y_parcelles,Result_Coords )

#### Changement de projection avec conversion  lambert 2 étendu vers lambert93 et stockage des nouvelles coordonnées ####

## Transformation en objet spatial

BIEN_AjustCoord_NewCoords <- st_as_sf(BIEN_AjustCoord %>% select(ID,x,y,Result_Coords) %>% filter (Result_Coords == "OriginalsCoords_NoParcellesCoords", !is.na (x) & !is.na (y)),
                     
                     coords = c("x", "y"),
                     
                     agr = "constant",
                     
                     crs = 27572,# Lambert 2 étendu
                     
                     stringsAsFactors = FALSE)

# 1 transaction éliminée
## conversion Lamb 93 et stockage coords
BIEN_AjustCoord_NewCoords <-  st_transform(BIEN_AjustCoord_NewCoords, crs = 2154)
coords <- st_coordinates(BIEN_AjustCoord_NewCoords)
BIEN_AjustCoord_NewCoords$Xlamb93<-coords[,1]   
BIEN_AjustCoord_NewCoords$Ylamb93<-coords[,2]

BIEN_AjustCoord_NewCoords <- as.data.frame(BIEN_AjustCoord_NewCoords)%>%
  select (-geometry, - Result_Coords)

## Mise en forme lignes avec coordonnées parcelles cadastrales
BIEN_AjustCoord_ParcellesCoords<- BIEN_AjustCoord %>% 
  select(ID,X_parcelles,Y_parcelles,Result_Coords)%>% 
  filter (Result_Coords != "OriginalsCoords_NoParcellesCoords")

BIEN_AjustCoord_ParcellesCoords<-BIEN_AjustCoord_ParcellesCoords %>%
  rename(Xlamb93 = X_parcelles) %>%
  rename(Ylamb93 = Y_parcelles) %>%
  select (-Result_Coords)

## Bindage des 2 tableaux

BIEN_CleanCoords <- rbind(BIEN_AjustCoord_NewCoords, BIEN_AjustCoord_ParcellesCoords)

# Arrange ID
BIEN_CleanCoords<-BIEN_CleanCoords %>% arrange(ID)
unique(duplicated(BIEN_CleanCoords$ID))
length(which(is.na(BIEN_CleanCoords$Xlamb93))) # Résulatat final : 31 transactions n'ont pas d'informations sur les coordonnées géographiques et seront écartées
# 
# df final
BIEN_BruteCom2 <- left_join(BIEN_BrutCom, BIEN_CleanCoords, by = "ID")
BIEN_BruteCom2$Geoloc <- ifelse(!is.na(BIEN_BruteCom2$Xlamb93), "Oui", "Non")
# table(BIEN_BruteCom2$Geoloc, BIEN_BruteCom2$REQ_ANC, useNA = "always")

#Ecrasement parcelles
rm(parcelles)

```

## Localisation des transactions dans les périmètres des terrains étudiés

```{r Localisation operations}

st_layers(dsn="APCP_Geom/APCP_Geom.gpkg",do_count = FALSE)$name
# Import des périmètres
Operations <- st_read("APCP_Geom/APCP_Geom.gpkg", quiet = TRUE, layer = "Operations")

#
Transactions_sf<- st_as_sf(BIEN_BruteCom2 %>%
                            filter (!is.na (Xlamb93) & !is.na (Ylamb93)),
                     
                     coords = c("Xlamb93", "Ylamb93"),
                     
                     agr = "constant",
                     
                     crs = 2154,# Lambert 93
                     
                     stringsAsFactors = FALSE)

 plot(st_geometry(Transactions_sf)) # apriori on a bien les transactions localisées sans erreurs
 
 # Jointure transactions avec les périmètres d'opérations
 
Transactions_Oper<- st_join(Transactions_sf, Operations, join = st_intersects, left=T)

table(Transactions_Oper$Nom_OP)

## Localisation des transactions dans les périmètres des terrains étudiés

```

## Redressement de l'échantillon pour les données concernées

```{r Redressement}

target <- c("1996", "1999", "2003","2004","2005","2006","2007","2008","2009","2010", "2011","2012","2018")

Transac_ForDupli <- Transactions_Oper %>%
  filter(annee %in% target)
  
Transac_Redress <- rbind(Transactions_Oper,
      Transac_ForDupli)

Transac_Redress<-Transac_Redress%>%
arrange(ID) %>%
  filter(!is.na(geometry))

Transac_Redress$ID <- row.names(Transac_Redress)
str(Transac_Redress)
table(Transac_Redress$Nom_OP)

# st_write(obj = Transac_Redress, dsn = "APCP_Outputs/APCP_BIEN.gpkg", layer = "Transactions",  delete_layer = TRUE, quiet = TRUE)

coords <- st_coordinates(Transac_Redress)
Transac_Redress$Xlamb93<-coords[,1]   
Transac_Redress$Ylamb93<-coords[,2]

Transac_Redress<- as.data.frame(Transac_Redress)%>%
  select(-geometry)

write.csv2(Transac_Redress, file = "APCP_Outputs/Transactions.csv", row.names = FALSE)

```